 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulateddevices;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import traffic.scenario.model.Node;
//import traffic.scenario.model.StreetLight;
import traffic.scenario.model.World;

/**
 *
 * @author Nathalia
 */
public class DeviceAgentBehaviorOnlyOne extends CyclicBehaviour {

    //ServerTCP server = new ServerTCP(1099);
    String adressGodAgent = "GOD";
    int numMsg = 0;
    DeviceAgent da;
    World traffic = World.getInstance();
    int time = 0;

    public DeviceAgentBehaviorOnlyOne(Agent a) {
        super(a);
        da = (DeviceAgent) a;
    }

    @Override
    public void action() {
        //   System.out.println("simulating device");
        //First Step:
        //To connect on the system (an adaptive agent is created for this
        //device
        //The device sends a message for GodAgent with its name and controllers type (previous
        //registered on FIoT system
        //The device waits for a signal from GodAgent with the adress/name
        //of its respective adaptive agent

        Map<String,String> messageFromGodAgent = new HashMap<>();
        List<Node> nodeList = new ArrayList<>();//new ArrayList<>(traffic.streetWorld.getLightList());
      //  for (Edge lane : laneList) {
           // String msgForGod = "Name:"+lane.getName()+";";
            String msgForGod = "";
            msgForGod += this.da.device.getNameController();
                     // String msgForGod = "";
            // msgForGod+=("-"+this.da.device.getSensorNameList());
            //msgForGod+=("-"+this.da.device.getActuatorNameList());
           //  System.out.println("Message for God: "+ msgForGod);
            try {
                this.da.sentMsgToSelectRecipient(adressGodAgent, msgForGod);
            } catch (FIPAException ex) {
                Logger.getLogger(DeviceAgentBehavior.class.getName()).log(Level.SEVERE, null, ex);
            }
            ACLMessage acmsg = null;
            while (acmsg==null) {

                acmsg = this.da.receiveMessage();
                if (acmsg != null) {
                    String msg = acmsg.getContent();
//                    messageFromGodAgent.put(lane.getName(), msg);
                     if (acmsg != null) {
                         msg = acmsg.getContent().replace("*", "");
                     }
                    messageFromGodAgent.put("lights", msg);
                //    System.out.println("Message from God: "+ msg);
                }
            }

        //}

        //System.out.println("Message from God for "+ this.da.device.getName()+ ":  "+ messageFromGodAgent);
        while (true) {
            /*Second step:
             The device sends a message for its adaptive agent
             with the data read from its sensors and wait for a message
             containing the data for actuator or just "OK" (when the device doesn't have
             any actuator
             The adaptive agent can send a message containing "WAIT" if the
             adaptive process is on*/

            String messageFromAdaptiveAgent = "";
            String msgForAdaptiveAgent = "";

            while (!messageFromAdaptiveAgent.contains("desconnect")) {

                while (time < this.traffic.getTimeSimulation()) {

                    if (World.getInstance().getPanel().isExecute()) {
                        //  
                        this.traffic.setFinishSimulation(false);
                        this.traffic.setActualTimeSimulation(time);
                        World.getInstance().getPanel().calculePeople();
                    //this.traffic.setFinishSimulation(false);
                        //boolean readd = World.getInstance().isReadOutput();
                        // boolean readd = this.traffic.isFinishSimulation();
                        //System.out.println("READD EH "+ readd);
                        // if (readd) {
                        // this.traffic.setFinishSimulation(true);
                        //  System.out.println("lane agent");
                        try {
                           // lightsList = new ArrayList<>(traffic.streetWorld.getLightList());
                           nodeList = new ArrayList<>(traffic.getAllNodeList());
                            for (Node node : nodeList) {
//                                DeviceAgent laneAg = new DeviceAgent(lane, "lanes", "laneContainer");
                                //StreetLight sl = traffic.streetWorld.getStreetLight(node.getName());
                                msgForAdaptiveAgent = traffic.streetWorld.readInputValue(node.getName());
                              //  DeviceAgent lightAg = new DeviceAgent(sl, "lights", "lightContainer");
                                //msgForAdaptiveAgent = lightAg.device.readValuesFromInputSensors();
                            //    System.out.println("MSg for Adaptive Agent");
                            //System.out.println(msgForAdaptiveAgent);
                                //     System.out.println("Message for Adaptive Agent from "+ laneAg.device.getName()+ ":  "+ msgForAdaptiveAgent);
                                // System.out.println("send message adaptive");
                                this.da.sentMsgToSelectRecipient(messageFromGodAgent.get("lights"),
                                        msgForAdaptiveAgent);
                                messageFromAdaptiveAgent = "";
                            //    System.out.println("Waiting msg from Adaptive Agent");
                                while (messageFromAdaptiveAgent == null
                                        || messageFromAdaptiveAgent.isEmpty()
                                        || messageFromAdaptiveAgent.contains("WAIT")) {
                                    acmsg = this.da.receiveMessage();
                                    if (acmsg != null) {
                                        messageFromAdaptiveAgent = acmsg.getContent().replace("*", "");
                            //            System.out.println("Message from Adaptive Agent"+messageFromAdaptiveAgent);
//                                        System.out.println("Name agent: "+ acmsg.getSender());
//                                        System.out.println("Name lane: "+lane.getName());
                                    }
                                }
                                //System.out.println("message from adaptive");
                                if (!messageFromAdaptiveAgent.contains("desconnect")
                                        || !messageFromAdaptiveAgent.contains("OK")) {
                                    int numberOfActuators = traffic.streetWorld.getNumberOfActuators(node.getName());
                                    if (numberOfActuators >= 1) {
                                        String[] value;
                                        // value = new String[this.da.device.getActuatorName().length];
                                        value = messageFromAdaptiveAgent.split(";");
                                        traffic.streetWorld.setActuatorValue(node.getName(), value);
                                     //   lightAg.device.setActuatorValue(value);
                                      //  lightAg.device.processActuatorInformation();
                                    }
                                    //        System.out.println("Message from Adaptive Agent for "+ laneAg.device.getName()+ ":  "+ messageFromAdaptiveAgent);
                                }
                                //this.traffic.incrementNumReadingOutput();
                            }

                        } catch (FIPAException ex) {
                            Logger.getLogger(DeviceAgentBehavior.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        //System.out.println("change semaphores");
                        World.getInstance().getPanel().changeLight();
                        //ou seja, se pedir ppra desenhar, o usuário ganha controle da execução,
                        //tendo que clicar em next
                        if (World.getInstance().getPanel().isDraw()) {
                            World.getInstance().getPanel().repaint();
                            World.getInstance().getPanel().setExecute(false);
                        }
                        //tem que botar false p poder usar o botão next
                        else{
                            World.getInstance().getPanel().setExecute(true);
                        }
                        time++;
                    }
                }

                time = 0;
                this.traffic.setActualTimeSimulation(time);
                this.traffic.setFinishSimulation(true);
                //System.out.println("reiniting");
                World.getInstance().getPanel().reinit();
                //System.out.println("calling observer ");
                this.observer();

            }
            // World.getInstance().setReadOutput(false);
        }
    }

    public void observer() {
        String messageFromObserverAgent = null;
//        String msgForGod = this.da.device.getName();
        String msgForObserver = "";
        String adressObserver = "OBSERVER";
//        while(!this.traffic.isFinishSimulation()){
//        
//        }
        //  if(this.traffic.isFinishSimulation()){

        msgForObserver += this.traffic.isFinishSimulation();
        msgForObserver += ";";
        msgForObserver += this.traffic.getTimeSimulation();
        msgForObserver += ";";
        //msgForObserver += this.traffic.getPercentConcludedPeople();
        msgForObserver += this.traffic.getNumOfPeopleCompletedAfterSimulation();
        msgForObserver += ";";
        msgForObserver += this.traffic.getTotalTimeTrip();
        msgForObserver += ";";
        msgForObserver += this.traffic.getTotalEnergy();
        msgForObserver += ";";
        msgForObserver += this.traffic.getNumPeople();
        msgForObserver += ";";
        msgForObserver += this.traffic.getNumRoads();
        //msgForObserver += this.traffic.getNumOfAmbulancesCompletedAfterSimulation();
        
     //   System.out.println("Time simulation: "+ this.traffic.getTimeSimulation());
     //   System.out.println("Total People: "+ this.traffic.getNumPeople());
     //   System.out.println("Total Street Lights: "+ this.traffic.getNumRoads());
     //   System.out.println("Num Completed People: "+ this.traffic.getNumOfPeopleCompletedAfterSimulation());
     //   System.out.println("Total time trip: "+ this.traffic.getTotalTimeTrip());
     //   System.out.println("Total energy: "+ this.traffic.getTotalEnergy());
        //System.out.println("Num Ambulances: "+this.traffic.getNumOfAmbulancesCompletedAfterSimulation());
        //      System.out.println("MESSAGE FOR OBSERVER "+ msgForObserver);
        try {
            this.da.sentMsgToSelectRecipient(adressObserver, msgForObserver);
        } catch (FIPAException ex) {
         //   Logger.getLogger(ControlAgentLoop.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (messageFromObserverAgent == null) {
            ACLMessage acmsg = this.da.receiveMessage();
            if (acmsg != null) {
                messageFromObserverAgent = acmsg.getContent();
                if (messageFromObserverAgent.contains("initSimulation")) {
                    this.traffic.setFinishSimulation(false);
                }

                //           System.out.println("MESSAGE FROM OBSERVER "+ messageFromObserverAgent);
            }
        }
        // }

    }

    public void read() {

        while (this.numMsg < 1) {
            ACLMessage msg = this.myAgent.receive();
            this.numMsg++;
            if (msg != null) {

                AID sender = msg.getSender();
                // System.out.println("Sender local name "+ sender.getLocalName());
                //insertVector(sender.getLocalName(), msg.getContent());
            }
        }

    }

}
